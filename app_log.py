#! env python
# -*- coding: utf-8 -*-
# lastest: 20190109

"""
MIT license
Copyright 2019 mm

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import os
import sys
import inspect
import datetime
from enum import IntEnum
from time import sleep
import re
import subprocess

"""
import app_log as log

log.critical("致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定。コンソールにその旨を表示させてアプリケーションを終了させる。")
log.fatal("致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定。スクリプトを再起動する。")
log.error("エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定");
log.warn("警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定")
log.info("情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき")
log.debug("デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定")
log.trace("トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定")

log.console("コンソールのみの表示。")
log.c("コンソールと同じ。入力の手間を省いたもの。")

# Format様式
log.c("test1 {1} {0} ", "1", "0")
log.c("test2", " 0 ", 1.0)
log.c("test3", ("0", 1))
log.c("test4 {:0>10} ", 1.02)
"""

DEFAULT_LOG_LEVEL_STRING = {"console": "CONSOLE", "trace": "TRACE", "debug": "DEBUG", "info": "INFO", "warn": "WARN",
                            "error": "ERROR", "fatal": "FATAL", "critical": "CRITICAL"}
# ログの色
# http://kaworu.jpn.org/kaworu/2018-05-19-1.php
LOG_COLOR = {'critical': "\033[31m", 'fatal': '\033[31m', 'error': '\033[31m', 'warn': '\033[35m', 'info': '\033[34m',
             'debug': '\033[36m', 'trace': '\033[33m', 'console': '\033[36m', 'end': '\033[0m'}

# 変数
# categoryはAppLogObjectで使用する、ファイルを保存する単純文字列のこと。
# このcategoryはAppLogObjectのtoメソッドで指定するフォルダ名も指す。
__log_key = "log"
# ファイルの拡張子
DEFAULT_FILE_EXT = ".log"


class LogType(IntEnum):
    """
    使用するログシステムは何にするか。
    SIMPLE == 単純なprint等で出力
    PYTHON == 組み込みロギングシステムを使う
    CUSTOM == それ以外。自身で書き込む必要がある。
    """
    SIMPLE = 0
    PYTHON = 1
    CUSTOM = 2


__use_log_type = LogType.SIMPLE


def set_log_type(value):
    """
    setter
    :param value:LogType
    :return:
    """
    global __use_log_type
    __use_log_type = value


def get_log_type():
    """
    getter
    :return:LogType
    """
    global __use_log_type
    return __use_log_type


if __use_log_type == LogType.PYTHON:
    import logging
    from logging import StreamHandler, Formatter, FileHandler
elif __use_log_type == LogType.CUSTOM:
    # todo ロギングシステムを別で用意する場合ここでインポートの書き換え
    pass

# このログを使うかどうか
__is_log = True


def is_log():
    """
    ログ出力をするかどうか確認
    :return:
    """
    return __is_log


def as_log(value=True):
    """
    ログ出力をするかどうかを設定
    :param value:bool
    :return:
    """
    global __is_log
    __is_log = value


# ファイル出力をするかどうか
__is_file = True


def is_file():
    """
    getter
    :return:bool
    """
    return __is_file


def as_file(value):
    """
    setter
    :param value:bool
    :return:
    """
    global __is_file
    __is_file = value


def get_function_info(back_count=0):
    """
    関数の呼び出し元を調べる関数。
    例えばget_function_info(2)でこの関数から見て2回前の関数の情報を取得する。
    https://docs.python.jp/3/library/inspect.html
    :param back_count:int #何回前の呼び出し元関数かの数値
    :return: filename, line_number, function_name, lines, index
    """
    inspect_data = inspect.currentframe().f_back
    for i in range(0, back_count):
        inspect_data = inspect_data.f_back
    return inspect.getframeinfo(inspect_data)


def get_datetime_format(format_str="%Y%m%d"):
    """
    現在の日付で文字列を返す。
    :return:str
    """
    return datetime.datetime.now().strftime(format_str)


def get_level_format(format_str="", log_level="info"):
    """
    ログレベルに合わせてフォーマットの文字列を返す。
    レベルの文字列は小文字で返る。
    例えば
    get_level_format("","info") == "info"
    get_level_format("log_level_{}","info") == "log_level_info"
    :param format_str:str # string.format()の様式
    :param log_level:str
    :return: str
    """
    return ("{}" if format_str == "" else format_str).format(DEFAULT_LOG_LEVEL_STRING[log_level.lower()].lower())


def __message_format_to_str(item):
    """
    入力値を文字列の形にして返す関数
    :param item:
    :return:
    """
    if isinstance(item, str):
        return item
    tmp = ""
    if isinstance(item, list) or isinstance(item, tuple):
        for i in item:
            tmp = tmp + __message_format_to_str(i) + " "
        return tmp
    elif isinstance(item, dict):
        for k, v in item.items():
            tmp = str(k) + " : " + __message_format_to_str(v) + " "
    else:
        try:
            tmp = str(item)
        except TypeError:
            tmp = item.__class__.__name__
    return tmp


def message_format(*message, function_inspect):
    """
    ログの様式を設定する場所
    第二引数のfunction_inspectはinspectを指定する。
    inspectを使うと、対象とする関数の属性を調べることができる。
    例えばinspect.currentframe()で現在のinspect、
    inspect.currentframe().f_backで１つ前のinspectを取得できるのでそれを使って関数の使用箇所などを取得する。
    :param function_inspect: # 関数のinspect
    :param message:str
    :return: str
    """
    # 呼び出し元の関数の内容を取得する
    filename, line_number, function_name, lines, index = inspect.getframeinfo(function_inspect)
    # 配列かつ文字列の場合format処理にする
    if len(message) > 1 and isinstance(message, tuple):
        if re.match(".*\{.*\}.*", message[0]) is not None and isinstance(message[0], str):
            try:
                tmp = message[0].format(*message[1:])
            except IndexError as e:
                tmp = "Log Format Error " + str(type(e)) + " : out of Range?"
        else:
            tmp = __message_format_to_str(message)
    else:
        tmp = __message_format_to_str(message)
    # todo メッセージの様式はここで設定している。
    return "{0}, {1} {2} {3} - ".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), line_number,
                                        os.path.basename(filename), function_name) + tmp


def get_default_log_dir(arg_category_path):
    """
    保存するファイルとフォルダの仕様を設定。
    # todo デフォルトのファイルパスの仕様を変更したい場合はここを編集
    :param arg_category_path:list #[[PathType,str]]
    :return:
    """
    return [[PathType.STRING, arg_category_path], [PathType.DATETIME, "%Y%m%d"]]  # 引数/日付.log


#
# 各ログレベル処理のデコレータ
# 必要な処理をf()の前後に追記することで処理前、処理後の動作を設定する。
# todo ログの処理の前後に必要な処理をここで設定
#

def _deco_console(f):
    """
    consoleのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        return tmp

    return wrapper


def _deco_critical(f):
    """
    criticalのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        print("---------------------------------------------------")
        # キー入力まち
        input('>>> スクリプトがクラッシュしました。エンターキーを押して終了します。')
        exit()
        return tmp

    return wrapper


def _deco_fatal(f):
    """
    fatalのデコレータ
    # todo 常時起動に使えると思いますが、無限ループになる危険性があります。
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        # todo 無限ループするならここをFalse
        if True:
            sleep(5)
            filename, line_number, function_name, lines, index = get_function_info(2)
            subprocess.Popen([sys.executable, filename])
            exit()
        return tmp

    return wrapper


def _deco_error(f):
    """
    errorのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        # raise BaseException # todo 例外を投げるならコメントアウト
        return tmp

    return wrapper


def _deco_warn(f):
    """
    warnのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        return tmp

    return wrapper


def _deco_info(f):
    """
    infoのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        return tmp

    return wrapper


def _deco_debug(f):
    """
    debugのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        return tmp

    return wrapper


def _deco_trace(f):
    """
    traceのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        tmp = f(*args, **kwargs)
        return tmp

    return wrapper


class PathType(IntEnum):
    """
    保存するパスの構造をどのようにするかを指定するenum。
    ここに項目を編集したら__get_path_str()の内容を追記・修正する。
    AppLog.__path_listで使用
    STRING == 単純文字列
    DATETIME == datetimeをstrftimeのフォーマットで
    LEVEL == info warnなどの小文字でログレベルの表記
    """
    STRING = 0
    DATETIME = 1
    LEVEL = 2


#
# ログの設定を保存するクラス
#


class AppLogObject(object):
    # 一時保管用
    __tmp_log_level = ""

    def __init__(self, name):
        # ファイルのパスを設定
        self.__path_list = get_default_log_dir(name)
        # ファイルの拡張子を設定
        self.ext = DEFAULT_FILE_EXT
        # 名前
        self.__name = name
        if get_log_type() == LogType.PYTHON:
            self.__logger_str = '%(levelname)s %(asctime)s - %(name)s - %(message)s'
            self.__logger = logging.getLogger(name)
            self.__logger.setLevel(logging.NOTSET)
            self.__stream_handler = StreamHandler()
            self.__stream_handler.setLevel(logging.NOTSET)
            self.__handler_format = Formatter(self.__logger_str)
            self.__stream_handler.setFormatter(self.__handler_format)
            self.__logger.addHandler(self.__stream_handler)
            self.__file_handler = None
        elif get_log_type() == LogType.CUSTOM:
            # todo 別で用意するロギングシステムの初期設定をここで設定する
            pass
        return

    def set_path_list(self, path_list):
        """
        # 基本様式
        # type1
        self.set_path_list([[PathType.STRING, "test"], [PathType.DATETIME, "%Y%m%d"]])
        self.get_path() == "test/20001231.log"
        # type2
        [[[PathType.STRING, "example"], [PathType.LEVEL, "{}"]], [PathType.DATETIME, "%Y%m%d"]]

        :param path_list:[[PathType,str]]
        :return:
        """
        self.__path_list = path_list

    def __get_path_str(self, list_object):
        """
        list_objectからパスに変換するスクリプト。
        例えば
        self.__get_path_str(
            [[PathType.DATETIME, "test%Y%m%d"], [PathType.LEVEL, "_{}"]]
        ) == "test20181231_info"
        :param list_object:[[PathType,str]]
        :return: str
        """
        if isinstance(list_object[0], list) or isinstance(list_object[0], tuple):
            path = ""
            for i in list_object:
                path = path + self.__get_path_str(i)
            return path
        elif isinstance(list_object[0], PathType):
            if list_object[0] == PathType.STRING:
                return list_object[1]
            elif list_object[0] == PathType.DATETIME:
                return get_datetime_format(list_object[1])
            elif list_object[0] == PathType.LEVEL:
                return get_level_format(list_object[1], self.__tmp_log_level)
        return ""

    def get_path(self):
        """
        ファイルのパスを返す。
        :return: str
        """
        path = ""
        for i in self.__path_list:
            path = os.path.join(path, self.__get_path_str(i))
        return path + self.ext

    def get_abs_path(self):
        """
        ファイルのパスを絶対パスで返す。
        :return: str
        """
        return os.path.abspath(self.get_path())

    def init(self):
        """
        ログを書き出して良いかどうかの判定と前処理
        返り値はログを書き出して良いかどうかの判定
        :return:
        """
        if is_log():
            if not os.path.exists(os.path.dirname(self.get_abs_path())) and is_file():
                os.makedirs(os.path.dirname(self.get_abs_path()))
            return True
        return False

    def print(self, *message, function_inspect, arg_is_file, arg_is_console, arg_log_level):
        """
        書き出し、表示に関してはここを修正。
        :param message: str list
        :param function_inspect: inspect
        :param arg_is_file: bool
        :param arg_is_console: bool
        :param arg_log_level: str
        :return:
        """
        self.__tmp_log_level = arg_log_level
        if self.init():
            if get_log_type() == LogType.SIMPLE:
                # 様式にしたがった文字列を作成。
                tmp = message_format(*message, function_inspect=function_inspect)
                # 単純ファイル出力
                if arg_is_file and is_file():
                    with open(self.get_abs_path(), 'a') as f:
                        print(arg_log_level + " " + tmp, file=f)
                # 単純コンソール出力
                if arg_is_console:
                    print(LOG_COLOR[arg_log_level.lower()] + arg_log_level + " " + tmp + LOG_COLOR["end"])
            elif get_log_type() == LogType.PYTHON:
                if self.__file_handler is None or self.__file_handler.baseFilename != self.get_abs_path():
                    # 前回のファイル設定を修正
                    if self.__file_handler is not None or not is_file():
                        self.__logger.removeHandler(self.__file_handler)
                    if is_file():
                        self.__file_handler = FileHandler(self.get_abs_path(), 'a')
                        self.__file_handler.setLevel(logging.NOTSET)
                        self.__file_handler.setFormatter(self.__handler_format)
                        self.__logger.addHandler(self.__file_handler)
                if arg_log_level.lower() == "fatal" or arg_log_level.lower() == "critical":
                    self.__logger.debug(*message)
                elif arg_log_level.lower() == "error":
                    self.__logger.error(*message)
                elif arg_log_level.lower() == "warn":
                    self.__logger.warning(*message)
                elif arg_log_level.lower() == "info":
                    self.__logger.info(*message)
                else:
                    self.__logger.debug(*message)
                    pass
            elif get_log_type() == LogType.CUSTOM:
                # todo 別で用意する場合はここに記述する
                pass

    #
    # ログレベル毎の処理
    # https://ja.wikipedia.org/wiki/Log4j
    #

    @_deco_critical
    def critical(self, *message):
        """
        log4jには無いエラーの種別
        致命的なエラー。fatalとの違いはコンソールで入力待の状態にして、エンターキーでアプリケーションを終了させて終わるかどうか。
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=True,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["error"]
                   )

    @_deco_fatal
    def fatal(self, *message):
        """
        致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定
        # アプリケーションを再起動させる。
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=True,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["error"],
                   )

    @_deco_error
    def error(self, *message):
        """
        エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=True,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["error"])

    @_deco_warn
    def warn(self, *message):
        """
        警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=True,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["warn"])

    @_deco_info
    def info(self, *message):
        """
        情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=True,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["info"])

    @_deco_debug
    def debug(self, *message):
        """
        デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定
        :param message: str
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=False,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["debug"])

    @_deco_trace
    def trace(self, *message):
        """
        トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定
        :return:
        """
        self.print(*message,
                   function_inspect=inspect.currentframe().f_back.f_back,
                   arg_is_file=True,
                   arg_is_console=False,
                   arg_log_level=DEFAULT_LOG_LEVEL_STRING["trace"])


app_log_object = {__log_key: AppLogObject(__log_key)}


def change_key(old_key, new_key):
    """
    app_log_objectのキーを変更する。
    :param old_key: str
    :param new_key: str
    :return:
    """
    global app_log_object
    app_log_object[new_key] = app_log_object[old_key]
    del app_log_object[old_key]
    global __log_key
    if old_key == __log_key:
        __log_key = new_key


def get_log_key():
    """
    現在のlog.のキーを返す。
    :return: str
    """
    global __log_key
    return __log_key


def set_log_key(key):
    """
    現在のlog.のキーを変更する。
    :param key: str
    :return:
    """
    global __log_key
    change_key(__log_key, key)


#
# AppLogのメンバを実行する関数
#


def to(arg_category):
    """
    カテゴリ（ファイルが入ったディレクトリ）を指定してAppLogObjectを返す。無い場合は作成する。
    :param arg_category: str
    :return:
    """
    if not arg_category in app_log_object:
        app_log_object[arg_category] = AppLogObject(arg_category)
    return app_log_object[arg_category]


#
# AppLogクラスのオブジェクトを実行するラッパー
#

def set_path_list(path_list):  app_log_object[__log_key].set_path_list(path_list)


def get_path(): return app_log_object[__log_key].get_path()


def get_abs_path(): return app_log_object[__log_key].get_abs_path()


#
# ログレベル毎の処理
#

@_deco_console
def console(*message):
    """
    コンソール表示のみ
    :param message: str
    :return:
    """
    if not __is_log:
        return
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=False,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["console"])


@_deco_console
def c(*message):
    if not __is_log:
        return
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=False,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["console"])


@_deco_critical
def critical(*message):
    """
    log4jには無いエラーの種別
    致命的なエラー。fatalとの違いはコンソールで入力待の状態にして、エンターキーでアプリケーションを終了させて終わるかどうか。
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["critical"])


@_deco_fatal
def fatal(*message):
    """
    致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定
    # アプリケーションを再起動させる。
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["fatal"])


@_deco_error
def error(*message):
    """
    エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["error"])


@_deco_warn
def warn(*message):
    """
    警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["warn"])


@_deco_info
def info(*message):
    """
    情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=True,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["info"])


@_deco_debug
def debug(*message):
    """
    デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=False,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["debug"])


@_deco_trace
def trace(*message):
    """
    トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定
    :param message: str
    :return:
    """
    app_log_object[__log_key].print(*message,
                                    function_inspect=inspect.currentframe().f_back.f_back,
                                    arg_is_file=True,
                                    arg_is_console=False,
                                    arg_log_level=DEFAULT_LOG_LEVEL_STRING["trace"])
