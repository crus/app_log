#! env python
# -*- coding: utf-8 -*-
__author__ = 'masayuki miki'
__date__ = "2018/12/01"

import os
import sys


def main():
    # 作業ディレクトリを自身のファイルのディレクトリに変更
    os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
    import app_log as log

    log.c("start -----------------")
    log.console("これはコンソールのみの表示。{1} {0} ", "様式に沿って書くことも出来る", "例えばstring.formatの")
    log.c("コンソールと同じ。入力の手間を省いたもの。")

    # 標準はlog/20181231.logの様式で保存される。
    log.trace("トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定")
    log.debug("デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定")
    log.info("情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき")
    log.warn("警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定")
    log.error("エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定");
    # log.fatal("致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定。スクリプトを再起動する。")
    # log.critical("致命的なエラーログ。コンソールにその旨を表示させてアプリケーションを終了させる。")

    # 様式の例
    log.c("test1 {1} {0} ", "1", "0")
    log.c("test2", " 0 ", 1.0, 2, 3)
    log.c("test3", ("0", 1))
    log.c("test4 {:0>10} ", 1.02)

    # ここからはより詳細な設定。普通は変えなくて良い。

    # log.info() は初期設定ではlog/20181231.logの様式でファイルを書き出す。
    # log.には初期設定として「log」という名前で指定されているので、log.to("log").とlog.は同じ意味。
    # toメソッドを用いれば別のフォルダに書き込むことが出来る。
    #  例えば以下のようなlog.to("フォルダ").info()のように書けば「フォルダ」に保存される。
    #  log.toメソッドは一時的に指定するだけで、log.の保存先を変えるにはlog.to.("log").set_path_listを書き換える必要がある。
    #  また、log.to().のキーを変更するにはchange_key()を使う。log.のキーを変更するにはset_log_key()を使う。
    log.to("log/system").info("別フォルダ")  # log/system/20181231.log

    # 初期設定では、日付.logのファイル名であるが、以下のような記述でファイルの階層構造を変える。
    log.to("log/system").set_path_list(
        [
            [log.PathType.STRING, "log"],  # => log/
            [[log.PathType.DATETIME, "%Y%m%d"], [log.PathType.LEVEL, "_{}"]],  # => 20001231_info/
            [log.PathType.STRING, "logfile"]  # => logfile
        ]
    )  # => log/system/20101231_info/logfile.log
    # なお、初期設定はget_default_log_dirに記述してあるので、必要があれば変更すれば良い。

    # 変えたフォルダ内に書き込む
    log.to("log/system").warn("さらにフォルダを変えたよ。")
    # このログのシステムを使わないようにするにはas_log(False)を設定。これ以降、as_log(True)で戻すまで何もしない。
    log.as_log(False)
    log.fatal("as_logをFalseにしたのでこれはログ出力されません。")
    log.c("end -----------------")
    return


if __name__ == '__main__':
    main()
