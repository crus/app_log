# Todo 修正したいところとか

- [ ] 特になし。

# Overview 概要

ログ出力用スクリプト。
Date: 2018/12/03

# ダウンロードページ

https://bitbucket.org/crus/app_log/downloads/

# Description 詳細

- ログを出力するときに出来るだけ自分が使いやすいようにするために作ったスクリプトです。
- log.info("内容")の様式で記述しておくと動作毎に所定の様式でコンソールの表示と.logファイルが出力されます。
- このスクリプトファイルをプロジェクトに組み込んで使用します。内部の仕様はスクリプトを直接書き換えてください。
- プロジェクトでログの仕様変更があって、別のログシステムや様式を使うことになっても、スクリプト内のinfoやwarnなどの関数名さえ同じであればスクリプトの内部だけリファクタリングすればそのまま使えるはずです。
- todo と記載されている部分は特に変更しやすい部分に関して説明しております。
- エラーレベルはlog4jをベースに作成。加えてスクリプトを終了させるcriticalというログレベルを追加。

# 実行環境

- Python 3.5以上を推奨。それ以下は未検証。
- 標準機能だけならば新たな外部パッケージは不要。

# 使い方

Pythonのプロジェクトフォルダに**app_log.py**をコピーして、`import app_log.py`もしくは`import app_log.py as log`で別名にして（推奨）使用。
以下、`log`でスクリプトを呼び出します。

# Example

## 基本

```python
# ファイルをプロジェクト内にコピーしてimport
import app_log as log

log.console("コンソールのみ表示")
log.c("コンソールと同じ。", "入力の手間を省いたもの。")

# 標準はlog/20181231.logの様式で保存される。
log.trace("トレース情報。更に詳細な情報。コンソールではなくログ上にだけ出力することを想定")
log.debug("デバッグ用の情報。システムの動作状況に関する詳細な情報。コンソールではなくログ上にだけ出力することを想定")
log.info("情報。実行時の何らかの注目すべき事象（開始や終了など）。コンソール等に即時出力することを想定。従ってメッセージ内容は簡潔に止めるべき")
log.warn("警告。廃要素となったAPIの使用、APIの不適切な使用、エラーに近い事象など、実行時に生じた異常とは言い切れないが正常とも異なる何らかの予期しない問題。コンソール等に即時出力することを想定")
log.error("エラー。予期しないその他の実行時エラー。コンソール等に即時出力することを想定");
# log.fatal("致命的なエラー。プログラムの異常終了を伴うようなもの。コンソール等に即時出力することを想定。スクリプトを再起動する。")
# log.critical("致命的なエラーログ。コンソールにその旨を表示させてアプリケーションを終了させる。")
```

## Format

strのformatのように記入出来ます。

```python
log.c("test1 {1} {0} ", "1", "0")
log.c("test2", " 0 ", 1.0)
log.c("test3", ("0", 1))
log.c("test4 {:0>10} ", 1.02)
```

## フォルダ・ファイル名変更

初期設定では**log/20181231.log**のように、**log**フォルダと日付番号.logの様式でファイルを書き出す。

初期設定を変えずに一時的に違うフォルダに保存したい場合は、toメソッドを使います。
例えば以下の場合は**log/system**というフォルダで書き出されます。

```python
log.to("log/system").info("test")
```

なお、初期設定として「log」という名前でフォルダ指定されているので、`log.to("log").メソッド`と`log.メソッド`は同じ意味です。

toメソッドは一時的に別のフォルダへ保存するものですが、以下のような記述でデフォルトの階層構造を変えることが出来ます。`log.set_path_list(内容)`を`log.to("test").set_path_list(内容)`に変更することで任意のフォルダの保存先を変えることも出来ます。
enumのPathTypeはペアとなる文字列の様式がどのような様式として記述されているかを指定するものです。

|名前|内容|入力値の例|入力値の例の結果|
|:---|:---|:---|:---|
|STRING|通常の文字列|log|log|
|DATETIME|時間のフォーマット| %Y%m%d|20190101|
|LEVEL|ログレベルの表示|{}|info|

例えば、下記の入力をすると、保存場所は`./log/system/20101231_info/logfile.log`のようになります。

```python
log.set_path_list(  # log.toでフォルダ変更
    [
        [log.PathType.STRING, "log"],  # => log/
        [[log.PathType.DATETIME, "%Y%m%d"], [log.PathType.LEVEL, "_{}"]],  # => 20001231_info/
        [log.PathType.STRING, "logfile"]  # => logfile
    ]
)  # => log/system/20101231_info/logfile.log
# なお、初期設定はget_default_log_dirに記述してあるので、必要があれば、その部分のスクリプトを直接変更すれば良い。
```

フォルダ名とtoメソッドで呼び出したいキーが違う場合もあるでしょう。
その場合は、change_keyメソッドを使います。

```python
log.to("old_name").change_key("new_name")
# old_nameに保存されるが、呼び出すときはnew_nameになるので、log.to("old_name")で呼び出そうとするとエラー。
log.to("new_name").info("test")
```

このchange_keyをlog.to("log")に指定すると、初期設定のキーも変更可能です。

# ログの種類

`LogType`には、どのログを使うかを定義しています。
app_log.py内の`__use_log_type`を直接書き換えるか、使用スクリプト側に`set_log_type`関数を使って変更出来ます。

|種類|内容|
|:---|:---|
|SIMPLE|printを使ったコンソール表示とファイル書き出しを行う。|
|PYTHON|Pythonのloggerを使用する。|
|CUSTOM|未定義。開発者がそれぞれ実装する。|

```python
# Pythonのloggerを使ったログ出力に変更。
log.set_log_type(log.LogType.PYTHON)
```

enum LogTypeの種類を`__use_log_type = LogType.CUSTOM`にして、`elif __use_log_type == LogType.CUSTOM:`以下の内容を書き換え、
AppLogObjectクラスのprintメソッド内、`elif get_log_type() == LogType.CUSTOM:`以下を修正します。

# スクリプトの直接編集

## 前処理・後処理

各ログレベルの処理の前後で何か別の処理を追加するには`__deco_XXX`に書きます。

```python
def _deco_console(f):
    """
    consoleのデコレータ
    :param f:
    :return:
    """

    def wrapper(*args, **kwargs):
        print("ここに前処理を書きます。")
        tmp = f(*args, **kwargs)
        print("ここに後処理を書きます。")
        return tmp

    return wrapper
```

# Licence ライセンス
[MIT](https://opensource.org/licenses/mit-license.php)